 #!/usr/bin/python
# -*- coding: UTF-8 -*-
from __future__ import division
import json
import pprint
import time
import calendar
import os
import re
import subprocess
import math
from time import strftime
from TogglPy import Toggl

###################
## CONFIGURATION ##
###################

from apiCredentials import apiCredentials

togglApiKey = apiCredentials.togglApiKey
togglWorkspaceId = apiCredentials.togglWorkspaceId

debug = 0

#Rounding
roundingResolution = 15/60 # Round to next quarter hour
#roundingResolution = 30/60  # Round to next half hour

roundingThreshold = 2.5/60 # 2.5 minutes -> decimal (2.5/60)
#roundingThreshold = 5/60 #  5 minutes -> decimal (  5/60)

###############
## FUNCTIONS ##
###############

def timeToDecimal(milliseconds):
	minutes = ((milliseconds / (1000*60)) % 60)
	hours = ((milliseconds / (1000*60*60)) % 24)
	if minutes != 0:
		time_decimal = float(hours+(minutes/60.000))
	else:
		time_decimal = float(hours)

def roundWithThreshold (value, resolution, threshold=1):
	return round((value + threshold) / resolution)*resolution

def slugify (value):
	keepcharacters = (' ','_','-')
	return "".join(c for c in value if c.isalnum() or c in keepcharacters).rstrip()

def tex_escape(text):
    """
        :param text: a plain text message
        :return: the message escaped to appear correctly in LaTeX
    """
    conv = {
        '&': r'\&',
        '%': r'\%',
        '$': r'\$',
        '#': r'\#',
        '_': r'\_',
        '{': r'\{',
        '}': r'\}',
        '~': r'\textasciitilde{}',
        '^': r'\^{}',
        '\\': r'\textbackslash{}',
        '<': r'\textless ',
        '>': r'\textgreater ',
    }
    regex = re.compile('|'.join(re.escape(unicode(key)) for key in sorted(conv.keys(), key = lambda item: - len(item))))
    return regex.sub(lambda match: conv[match.group()], text)

#################
## DEFINITIONS ##
#################

months =  {1:'Januar',2:'Februar',3:'März',4:'April',5:'Mai',6:'Juni',7:'Juli',8:'August',9:'September',10:'Oktober',11:'November',12:'Dezember'}
years = {16:'2016',17:'2017',18:'2018'}

##############
## LET's GO ##
##############

toggl = Toggl()
toggl.setAPIKey(togglApiKey)

# time scope
year = input('Für welches Jahr soll der Tätigkeitsnachweis generiert werden? (16-18): 20')
timeMode = str(input('Monats (0)- oder Quartalsbericht (1): '))
if timeMode == '0':
	month = input('Für welchen Monat soll der Tätigkeitsnachweis generiert werden? (1-12):   ')
elif timeMode == '1':
	quarter = input('Für welches Quartal soll der Tätigkeitsnachweis generiert werden? (1-4)   Q')

# client scope

clients = toggl.getClients()
i = 1
for client in clients:
	print str(i) + ": " + client['name']
	i += 1

filterClient = input('Für welchen Kunden sollen die Daten ausgewertet werden? : ')-1
clientId = clients[filterClient]['id']
clientName = clients[filterClient]['name'].encode('utf-8')

# project scope

projects = toggl.request("https://www.toggl.com/api/v8/clients/"+str(clientId)+"/projects")

i = 1
for project in projects:
	print str(i) + ": " + project['name']
	i += 1

filterProject = input('Welches Projekt soll ausgewertet werden? : ')-1
projectId = projects[filterProject]['id']
projectName = projects[filterProject]['name'].encode('utf-8')


# SLA
isSLA = bool(input("Handelt es sich um einen Tätigkeitsnachweis für einen SLA? (1/0) : "))
if isSLA is True:
	timeBudgetSLA = input("Besteht ein SLA Budget? (z.B. 12.5 für 12,5 Stunden/Monat)  : ")
	if timeBudgetSLA is None:
		timeBudgetSLA = 0

data = {
	'workspace_id': togglWorkspaceId,
	#'client_ids': clientId,
	'project_ids': projectId,
	'billable': True,
	'order_desc': False,
	'tag_ids': 0

}

if timeMode == "0":
	data.update({
	'since': years[year] + "-" + str(month).zfill(2) + "-01",
	'until': years[year] + "-" + str(month).zfill(2) + "-" + str(calendar.monthrange(int(years[year]),month)[1]),
	})
if timeMode == "1":
	firstMonth = ((quarter-1)*3)+1
	lastMonth = ((quarter-1)*3)+3
	data.update({
	'since': years[year] + "-" + str(firstMonth).zfill(2) + "-01",
	'until': years[year] + "-" + str(lastMonth).zfill(2) + "-" + str(calendar.monthrange(int(years[year]),lastMonth)[1]),
		})

all_entries = []
current_page = 1

report = toggl.getDetailedReport(data)

while (int(math.ceil(float(report['total_count'])/report['per_page'])) >= current_page):
	data['page'] = current_page
	report = toggl.getDetailedReport(data)
	all_entries.extend(report['data'])
	current_page+=1

#Generate the table of time entries
table = ''
time_total = float(0)
tasks = []

for entry in all_entries:
	minutes = (entry['dur'] // (1000*60)) % 60
	hours = ((entry['dur'] // (1000*60*60)) % 24)
	if minutes != 0:
		time_decimal = hours+(minutes/60.000)
	else:
		time_decimal = hours

	timeDecimal_rounded = roundWithThreshold(time_decimal,roundingResolution,roundingThreshold)

	time_total += float(timeDecimal_rounded)
	if timeDecimal_rounded > 0:
		task = entry['task'] or ''
		table += tex_escape(entry['description'].replace(task+" - ", '').encode('utf-8')) + " & " + \
			 	 tex_escape(task.encode('utf-8')) + " & " + \
			 	 strftime('%d.%m.%Y',time.strptime(entry['start'][:-6],'%Y-%m-%dT%H:%M:%S')) + " & " + \
			  	 format(timeDecimal_rounded,'.2f') + " \\\\  \\hline\n"

		# if entry['tid']:
		# 	if tasks[entry['tid']]:
		# 		tasks[entry['tid']] += timeDecimal_rounded
		# 	else:
		# 		tasks[entry['tid']] = timeDecimal_rounded

template = open('templates/template.tex', 'r').read()
report   = template.replace('||year||', years[year]).\
					replace('||timeTotal||', str(format(time_total,'.2f'))).\
					replace('||customer||', clients[filterClient]['name'].encode('utf-8')).\
					replace('||table||', table).\
					replace('||project||', projectName)

if isSLA:
	if timeBudgetSLA-time_total < 0:
		slatable = open('templates/slatable_Mehraufwand.tex','r').read()
	else:
		slatable = open('templates/slatable.tex','r').read()

	report = report.replace('||sla||', slatable).\
		   	replace('||timeSLA||', str(timeBudgetSLA)).\
			replace('||timeLeft||', str(format(abs((timeBudgetSLA-time_total)), '.2f'))).\
			replace('||year||', years[year]).\
			replace('||timeTotal||', str(format(time_total,'.2f')))
else:
	report = report.replace('||sla||', ' ')


if timeMode == '0':
	report = report.replace('||month||', months[month])
elif timeMode == '1':
	report = report.replace('||month||', str(quarter))

reportsPath = 'reports/'+slugify(clientName)+'/'


if timeMode == '0':
	filename = 	years[year]+"-"+str(month).zfill(2)+"_"+slugify(projectName)
elif timeMode == '1':
	filename = 	years[year]+"-Q"+str(quarter)+"_"+slugify(projectName)


if not os.path.exists(reportsPath):
    os.makedirs(reportsPath)

outFile = open(reportsPath+filename+".tex", "w+")
outFile.write(report)
outFile.close()

time.sleep(1)

cmd = ['/Library/TeX/texbin/xelatex', filename+".tex"]
proc = subprocess.Popen(cmd, cwd=os.path.dirname(os.path.realpath(__file__)) +"/"+reportsPath)
time.sleep(3)
proc = subprocess.Popen(cmd, cwd=os.path.dirname(os.path.realpath(__file__)) +"/"+reportsPath)
proc.communicate()

time.sleep(1)
if not debug:
	os.unlink(os.path.dirname(os.path.realpath(__file__)) +"/"+reportsPath+filename+".tex")
	os.unlink(os.path.dirname(os.path.realpath(__file__)) +"/"+reportsPath+filename+".aux")
	os.unlink(os.path.dirname(os.path.realpath(__file__)) +"/"+reportsPath+filename+".log")
